import React from 'react'
import {isEmpty}  from 'lodash'
import {Modal,Button,Container,Grid,Input, Label, TextArea} from 'semantic-ui-react'
const {Row,Column} = Grid
class ModalEditCooking extends React.Component{
    state={
        isCreate:true,
        embeded: "",
        name: "",
        related: [],
        // 0: {link: "/product-list", name: "ไส้กรอก"}
        // 1: {link: "/product/23072524", name: "ไส้กรอกไก่จูเนียร์สุพรีมชีส"}
        updateBy:'',
        updateTime:'',
    }
    componentDidMount(){
      const {cookingData} = this.props
      console.log('cookingData',cookingData)
      if(!isEmpty(cookingData)){
        this.setState({...cookingData,
            isCreate:false,
        })
      }
    }

    onSubmit(){
        const date = new Date()
        const {
            related,
            embeded,
            isCreate,
            name,
            updateBy
        } = this.state;
        let submitData = {
            updateBy,
            updateTime:date,
            embeded,
            name,
            related
        }
        // console.log(date);
        if (isCreate){
          this.props.onSubmitNew(submitData)
        }else{
          this.props.onSubmit(submitData);
        }
        
    }
    onChange(value,field){
        this.setState({[field]:value})
    }
    onChangeRelate(value,field,idx){
      const {related} = this.state;
      related[idx][field] = value
      this.setState({related})
    }
    addRelate(){
      const {related} =this.state
      related.push({
        link:'',
        name:'',
      })
      this.setState(related)
    }
    render(){
        const {name,
            embeded,
            related,
            updateBy,
            updateTime,
            isCreate
        } = this.state;
        console.log(related)
        const {isOpen,onClose,onDelete} = this.props
        return <Modal
            open={isOpen}
        >
        <Modal.Header>สร้าง / แก้ไข หน้าทำอาหาร</Modal.Header>
        <Modal.Content>
            <Container>
                <Grid>
                    <Row>
                        <Column fluid>
                            <Label>Link Video :</Label>
                            <Input
                                fluid
                                placeholder='Link Video' 
                                value={embeded}
                                onChange={(e)=>{this.onChange(e.target.value,'embeded')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                      <Column fluid>
                            <Label>name cooking :</Label>
                            <Input
                                fluid
                                value={name}
                                placeholder='Name Cooking' 
                                onChange={(e)=>{this.onChange(e.target.value,'name')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                      <Column fluid>
                          <Label>Relate link</Label>
                          <Button basic onClick={()=>{this.addRelate()}}>Add</Button>
                          {related.map((item,i)=>(
                            <Row>
                              <div>{`Relate #${i+1}`}</div>
                              <Column width={5} style={{width:'50%',display:'inline-block'}}>
                              <Label>Relate link</Label>
                                <Input
                                  value={item.link}
                                  placeholder='link relate' 
                                  onChange={(e)=>{this.onChangeRelate(e.target.value,'link',i)}}
                                />
                                </Column>
                              <Column  width={5} style={{width:'50%',display:'inline-block'}}>
                                <Label>Relate Name</Label>
                                <Input
                                  value={item.name}
                                  placeholder='name relate' 
                                  onChange={(e)=>{this.onChangeRelate(e.target.value,'name',i)}}
                                />
                              </Column>
                            </Row>
                          ))}
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                                <Label>แก้ไขโดย :</Label>
                                <Input
                                    onChange={(e)=>{this.onChange(e.target.value,'updateBy')}}
                                    placeholder='แก้ไขโดย' 
                                 />
                        </Column>
                        <Column width={10}>
                          <p>แก้ไขล่าสุด โดย {updateBy} เมื่อ {updateTime} (TIMEZONE + 7)</p>
                        </Column>
                    </Row>
                </Grid>
            </Container>
        </Modal.Content>
        <Modal.Actions>
          {!isCreate && <Button style={{"float":"left"}} onClick={()=>onDelete()}  negative>
            ลบ
          </Button>}
          <Button onClick={()=>onClose()}  negative>
            ยกเลิก
          </Button>
          <Button
            positive
            labelPosition='right'
            icon='checkmark'
            onClick={()=>this.onSubmit()}
            content='บันทึก'
          />
        </Modal.Actions>
      </Modal>
    }
}

export default ModalEditCooking