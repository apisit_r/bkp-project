import React from 'react'
import {isEmpty}  from 'lodash'
import {Modal,Button,Container,Grid,Input, Label, TextArea,Checkbox,Dropdown} from 'semantic-ui-react'
const {Row,Column} = Grid
const AWS = require('aws-sdk');
const bluebird = require('bluebird');
const uuidv1 = require('uuid/v1');
const ENV = {
    S3_BUCKET:'bkp-picture',
    AWS_ACCESS_KEY_ID:'AKIATOAVWHVHQCAHPI2E',
    AWS_SECRET_ACCESS_KEY:'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',
    PREFIX_URL:'https://bkp-picture.s3-ap-southeast-1.amazonaws.com/'
  }
  AWS.config.update({
    accessKeyId:ENV.AWS_ACCESS_KEY_ID,
    secretAccessKey: ENV.AWS_SECRET_ACCESS_KEY,
    region: 'us-east-2',
  });
  
  // configure AWS to work with promises
  AWS.config.setPromisesDependency(bluebird);
  let reader 
class ModalEditContent extends React.Component{
    state={
        isHide:false,
        isCreate:true,
        breadCrumb : '',//done
        category : '',//done
        contact : {
          email : "",
          tel : ""
        },//done
        email:'',
        tel:'',
        packpic:'',
        foodpic:'',
        picUrl : ['',''],//done
        productId : '',//done
        productName : '',//done
        slogan : '',//done
        ingredients:'',
        cookMethod:'',
        keepMethod:'',
        storageMethod:'',
        factoryLocation:'',
        distributions:[],
        price:'',
        weight:'',
        allergy:'',
        updateBy:'',
        updateTime:'',
        th : {
          firstTab : {
            ingredients : {
              data : "",
              label : "ส่วนประกอบ"
            },
            cookMethod : {
              data: "",
              label : "วิธีการปรุง"
            },
            keepMethod : {
              data : '',
              label : 'วิธีจัดเก็บ'
            },
            storageMethod : {
              data : "",
              label : "วิธีเก็บรักษา"
            },
            factoryLocation : {
              data : "",
              label : 'สถานที่ผลิต',
            }
          },
          fouthTab : {
            distributions : {
              data : [ ],
              label : 'สถานที่จำหน่าย'
            },
            price : {
              data : '',
              label : 'ราคา'
            }
          },
          secondTab : {
            allergy:{
              data : "",
              label : "ข้อมูลผู้แพ้อาหาร"
            },
            weight : {
              data : "",
              label : 'น้ำหนัก'
            }
          },
        }
    }
    componentDidMount(){
      const {productData} = this.props
      if(!isEmpty(productData)){
        const {
          picUrl=['',''],
          contact:{
            email,
            tel,
          },
          th : {
            firstTab,
            fouthTab:{
              distributions,
              price,
            },
            secondTab:{
              weight,
              allergy,
            }
          }
        } = productData
        const packpic= picUrl[0]
        const foodpic =picUrl[1]
        const ingredients=firstTab['1-ingredients']
        const cookMethod=firstTab['2-cookMethod']
        const keepMethod=firstTab['3-keepMethod']
        const storageMethod=firstTab['4-storageMethod']
        const factoryLocation=firstTab['5-factoryLocation']
        this.setState({...productData,
            foodpic,
            packpic,
            email,
            tel,
            ingredients:ingredients.data || '',
            cookMethod:cookMethod.data || '',
            keepMethod:keepMethod.data || '',
            storageMethod:storageMethod.data || '',
            factoryLocation:factoryLocation.data || '',
            distributions:distributions.data|| [],
            price:price.data || '',
            weight:weight.data || '',
            allergy: allergy ? allergy.data : '',
            isCreate:false,
        })
      }
    }

    onSubmit(){
        const date = new Date()
        const {
          isHide,
            productName,
            slogan,
            breadCrumb,
            category,
            email,
            tel,
            picUrl,
            productId,
            ingredients ,
            cookMethod ,
            keepMethod ,
            storageMethod ,
            factoryLocation,
            distributions ,
            price,
            weight,
            allergy,
            isCreate,
            name,
            packpic,
            foodpic
        } = this.state;
        let submitData = {
            isHide,
            updateBy:name,
            updateTime:date,
            productName,
            slogan,
            breadCrumb,
            category,
            contact:{
                email,
                tel,
            },
            picUrl:[packpic,foodpic],
            productId:+productId,
            th : {
                
                firstTab : {
                },
                fouthTab : {
                  distributions : {
                    data : distributions,
                    label : 'สถานที่จำหน่าย'
                  },
                  price : {
                    data : price,
                    label : 'ราคา'
                  }
                },
                secondTab : {
                  allergy:{
                    data:allergy,
                    label:'ข้อมูลผู้แพ้อาหาร'
                  },
                  weight : {
                    data : weight,
                    label : 'น้ำหนัก'
                  }
                },
            }
        }
        submitData.th.firstTab['1-ingredients'] ={
          data : ingredients,
          label : "ส่วนประกอบ"
        }
        submitData.th.firstTab['2-cookMethod'] ={
          data: cookMethod,
          label : "วิธีการปรุง"
        }
        submitData.th.firstTab['3-keepMethod'] ={
          data : keepMethod,
          label : 'วิธีจัดเก็บ'
        }
        submitData.th.firstTab['4-storageMethod'] ={
          data : storageMethod,
          label : 'วิธีจัดเก็บ'
        }
        submitData.th.firstTab['5-factoryLocation'] ={
          data : factoryLocation,
          label : 'วิธีจัดเก็บ'
        }
        console.log(submitData);
        // if (isCreate){
        //   this.props.onSubmitNew(submitData)
        // }else{
        //   this.props.onSubmit(submitData);
        // }
        
    }
    onChange(value,field){
        this.setState({[field]:value})
    }
    // onChangeArea(value,field){
    //     this.setState({[field]:value.split(',')})
    // }
    // displayTextArea(value){
    //     return value.join(',')
    // }
    uploadFile = (buffer, name, type,field) => {
      const params = {
        ACL: 'public-read',
        Body: buffer,
        Bucket: ENV.S3_BUCKET,
        Key: `${name}.${type}`,
      };
      const s3 = new AWS.S3();
      const s3UploadPromise = new Promise(function(resolve, reject) {
          s3.upload(params, function(err, data) {
              if (err) {
                  reject(err);
              } else {
                  resolve(data);
              }
              });
      })
      const url = s3UploadPromise.then((result)=>{
          const path = `${ENV.PREFIX_URL}${name}.${type}`
          this.setState({[field]:path})
      })
    };
    handleEvent = (field)=>{
      const buffer =  reader.result
      const name = uuidv1()
      this.uploadFile(buffer, name, 'png',field)
    }
    handleUpload = (e,field) => {
      console.log(e,field)
      var file = e.target.files[0];
      reader = new FileReader();
      reader.onload = (e)=>{this.handleEvent(field)}
      reader.readAsArrayBuffer(file);
    }
    onChangeDropDown=(value,field)=>{
      this.setState({[field]:value})
    }
    render(){
      const options =[
        { key: 'angular', text: '7-11', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/7-11+logo-01.jpg' },
        { key: 'css', text: 'BigC', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/LOGO+BIG+C-01.jpg' },
        { key: 'Bkp', text: 'Bkp', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/BKP-01.jpg' },
        { key: 'cj', text: 'Cj', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/CJ+SuperMarket-01.jpg' },
        { key: 'cp', text: 'CP', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/CP+freshmart-01.jpg' },
        { key: 'goumet', text: 'Goumet', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Gourmet-01.jpg' },
        { key: 'jiffy', text: 'Jiffy', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Jiffy-11.jpg' },
        { key: 'lawson', text: 'Lawson108', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Lawson108+Logo.png' },
        { key: 'fm', text: 'Famiry Mart', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Logo+FamilyMart-01.jpg' },
        { key: 'mv', text: 'max valu', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Maxvalu-01.jpg' },
        { key: 'tm', text: 'The Mall', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Themalllogo.jpg' },
        { key: 'tl', text: 'Tesco Lotus', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Tesco+Lotus+-01.jpg'},
        { key: 'tops', text: 'Tops', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Tops+market-01.jpg' },
        { key: 'villa', text: 'Villa', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/Villa+Market+LOGO-1.png' },
        { key: 'gp', text: 'Golden  place', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/golden+place-01.jpg' },
        { key: 'CFH', text: 'CFH', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/logo+CFH2-01.jpg' },
        { key: 'makro', text: 'makro', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/makro-01.jpg' },
        { key: 'rimping', text: 'rimping', value: 'https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/shop_logo/rimping+logo.jpg' },
      ]
        const {
          isHide,
          productName,
            slogan,
            breadCrumb,
            category,
            email,
            tel,
            picUrl,
            productId,
            ingredients ,
            cookMethod ,
            keepMethod ,
            storageMethod ,
            factoryLocation,
            distributions ,
            price,
            weight,
            allergy,
            updateBy,
            updateTime,
            isCreate
        } = this.state;
        const {isOpen,onClose,onDelete} = this.props
        return <Modal
            open={isOpen}
        >
        <Modal.Header>สร้าง / แก้ไข สินค้า</Modal.Header>
        <Modal.Content>
            <Container>
                <Grid>
                  <Row>
                    <Column>
                      <Label>Hide Product :</Label>
                      <Checkbox toggle
                       checked={isHide}
                       onChange={(e)=>{this.onChange(!isHide,'isHide')}}
                       />
                    </Column>
                  </Row>
                    <Row>
                        <Column width={6}>
                            <Label>Product Name :</Label>
                            <Input
                                placeholder='Product Name' 
                                value={productName}
                                onChange={(e)=>{this.onChange(e.target.value,'productName')}}
                            />
                        </Column>
                        <Column width={8}>
                            <Label>Slogan Product :</Label>
                            <Input
                                value={slogan}
                                placeholder='Slogan Product' 
                                onChange={(e)=>{this.onChange(e.target.value,'slogan')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                            <Label>SKU :</Label>
                            <Input
                                value={productId}
                                placeholder='productId' 
                                onChange={(e)=>{this.onChange(e.target.value,'productId')}}
                            />
                        </Column>
                        <Column width={8}>
                            <Label>bread-crumb :</Label>
                            <Input
                                value={breadCrumb}
                                placeholder='bread-crumb' 
                                onChange={(e)=>{this.onChange(e.target.value,'breadCrumb')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                                <Label>Category :</Label>
                                <Input
                                    value={category}
                                    placeholder='category' 
                                    onChange={(e)=>{this.onChange(e.target.value,'category')}}
                                />
                            </Column>
                        <Column width={10}>
                            <Label>Contact Email:</Label>
                            <Input
                                placeholder='Email'
                                value={email}
                                onChange={(e)=>{this.onChange(e.target.value,'email')}}
                            />
                            <Input
                                value={tel}
                                placeholder='Tel' 
                                onChange={(e)=>{this.onChange(e.target.value,'tel')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={8}>
                                <Label>pack pic :</Label>
                                <Input
                                    fluid
                                    type="file"
                                    onChange={(e)=>this.handleUpload(e,'packpic')}/>
                                <img width={100} height={100} src={this.state.packpic} ></img>
                        </Column>
                        <Column width={8}>
                                <Label>food pic :</Label>
                                <Input
                                    fluid
                                    type="file"/>
                                     onChange={(e)=>this.handleUpload(e,'foodpic')}/>
                                <img width={100} height={100} src={this.state.foodpic}></img>
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                                <Label>ส่วนประกอบ :</Label>
                                <Input
                                  onChange={(e)=>{this.onChange(e.target.value,'ingredients')}}
                                 value={ingredients} placeholder='ส่วนประกอบ'/>
                        </Column>
                        <Column width={6}>
                                <Label>วิธีการปรุง :</Label>
                                <Input
                                  onChange={(e)=>{this.onChange(e.target.value,'cookMethod')}}
                                 value={cookMethod}  placeholder='วิธีการปรุง'/>
                              
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                                <Label>วิธีจัดเก็บ :</Label>
                                <Input
                                    onChange={(e)=>{this.onChange(e.target.value,'keepMethod')}}
                                 value={keepMethod} placeholder='วิธีจัดเก็บ'/>
                            
                        </Column>
                        <Column width={6}>
                                <Label>วิธีเก็บรักษา :</Label>
                                <Input
                                    onChange={(e)=>{this.onChange(e.target.value,'storageMethod')}}
                                 value={storageMethod} placeholder='วิธีเก็บรักษา'/>
                            
                        </Column>
                    </Row>
                    <Row>
                    <Column width={6}>
                                <Label>สถานที่ผลิต:</Label>
                                <Input
                                 onChange={(e)=>{this.onChange(e.target.value,'factoryLocation')}}
                                 value={factoryLocation} placeholder='สถานที่ผลิต'/>
                               
                        </Column>
                        <Column width={5}>
                                <Label>ราคา :</Label>
                                <Input 
                                onChange={(e)=>{this.onChange(e.target.value,'price')}}
                                value={price} placeholder='ราคา'/>
                        </Column>
                        <Column width={5}>
                                <Label>น้ำหนัก :</Label>
                             
                                <Input 
                                   onChange={(e)=>{this.onChange(e.target.value,'weight')}}
                                 value={weight} placeholder='น้ำหนัก'/>
                        </Column>
                    </Row>
                    <Row>
                        <Column width={16}>
                            <Label>ข้อมูลผู้แพ้อาหาร :</Label>
                             <Input 
                                fluid
                                onChange={(e)=>{this.onChange(e.target.value,'allergy')}}
                                value={allergy} placeholder='ข้อมูลผู้แพ้อาหาร'/>
                        </Column>
                    </Row>
                    <Row>
                        <Column width={16}>
                                <Label>สถานที่จำหน่าย :</Label>
                                <Dropdown 
                                placeholder='สถานที่จำหน่าย' 
                                fluid
                                 multiple 
                                 selection 
                                 value={this.state.distributions}
                                 onChange={(e,data)=>{this.onChangeDropDown(data.value,'distributions')}}
                                 options={options} />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                                <Label>แก้ไขโดย :</Label>
                                <Input
                                    onChange={(e)=>{this.onChange(e.target.value,'name')}}
                                    placeholder='แก้ไขโดย' 
                                 />
                        </Column>
                        <Column width={10}>
                          <p>แก้ไขล่าสุด โดย {updateBy} เมื่อ {updateTime} (TIMEZONE + 7)</p>
                        </Column>
                    </Row>
                </Grid>
            </Container>
        </Modal.Content>
        <Modal.Actions>
          {!isCreate && <Button style={{"float":"left"}} onClick={()=>onDelete()}  negative>
            ลบ
          </Button>}
          <Button onClick={()=>onClose()}  negative>
            ยกเลิก
          </Button>
          <Button
            positive
            labelPosition='right'
            icon='checkmark'
            onClick={()=>this.onSubmit()}
            content='บันทึก'
          />
        </Modal.Actions>
      </Modal>
    }
}

export default ModalEditContent