import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Grid,
  Image,
  Divider,
} from 'semantic-ui-react';
import {Cookies} from 'react-cookie'


const styled = require('styled-components').default;


const CardContent = ({name,description,sku})=>{
    return (
        <CardContentStyle>
            <div>
            <p className="card-name">{name}</p>
            </div>
            <Divider />
            <div>
                <p className="card-desc"> {description}</p>
            </div>     
            <div  className="more-detail-box">
                <a className="more-detail"><span>More Detail</span></a>
            </div>
        </CardContentStyle>
    )
}
const CardContentStyle = styled.div `
.card-name{
    font-size:16px;
    font-weight:bold;
    font-family: 'Kanit', sans-serif; 
}
.card-desc{
    font-size:14px;
    font-family: 'Kanit', sans-serif; 
    word-break:break-word;
}
.more-detail{
    font-family: 'Kanit', sans-serif; 
    font-size:10px;
    color:#EA4200;
}
.more-detail-box{
    text-align:right;
}
`

class ProductCard extends React.Component {
    handleOnRedirect = (sku,cat) => {
        const height = window.scrollY
        const cookies = new Cookies();
        cookies.set('prvHeight',height)
        
        window.location.href = '/product/'+sku+'?cat='+cat
    }
    render() {
    const {name="ProductName" 
    ,description="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
     ,sku="1"
    ,cat=''} = this.props
      return (
          <ProductCardStyle>
              <Container>
                  <Grid className="product-card">
                      <Grid.Row
                        onClick={()=>this.handleOnRedirect(sku,cat)}
                        >
                        <Grid.Column  className="product-image"  width={7}>
                            <Image src={this.props.picture} />
                        </Grid.Column>
                        <Grid.Column width={9}>
                           <CardContent
                           name={name}
                           description={description}
                           sku={sku}
                           />
                        </Grid.Column>
                      </Grid.Row>
                  </Grid>
              </Container>
          </ProductCardStyle>
      );
    }
}
  
const ProductCardStyle = styled.div `
.product-card{
    border:1px white solid;
    border-radius:15px;
    box-shadow:4px 5px 8px 1px #DCDCDC;
    background: white;
    min-height:240px;
}
.product-image{
    margin:auto !important;
}
`
ProductCard.propTypes = {
    picture: PropTypes.string,
}
export default (ProductCard);