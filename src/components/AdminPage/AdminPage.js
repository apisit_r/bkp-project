import React from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment,Modal } from 'semantic-ui-react'
import {Cookies} from 'react-cookie'
import AdminPanel from '../AdminPanel';

class LoginForm extends React.Component{
    state={
        username:'',
        passsword:''
    }
    onChange=(value,field)=>{
        this.setState({[field]:value})
    }
render(){
return(
  <div className='login-form'>
    {/*
      Heads up! The styles below are necessary for the correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
    <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}
    </style>
    <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          Log-in to your account
        </Header>
        <Form size='large'>
          <Segment stacked>
            <Form.Input 
            fluid icon='user' 
            iconPosition='left' 
            placeholder='E-mail address' 
            onChange={(e)=>{
                this.onChange(e.target.value,"username")
                }
            }
            />
            <Form.Input
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              type='password'
              onChange={(e)=>{
                this.onChange(e.target.value,"password")
                }
            }
            />
            <Button color='teal' fluid size='large' onClick={()=>this.props.onLogin(this.state.username,this.state.password)}>
              Login
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  </div>
)}
}
class AdminPage extends React.Component{
    state={
        isLogin:false,
        username:'',
        password:'',
    }

    onLogin=(username,password)=>{
      if(username==='adminbkp' && password==='P@ssw0rdBkp'){
            this.setState({isLogin:true})
            alert('ยินดีต้อนรับสู่ ADMIN PAGE')
            
        }else{
            alert('ีรหัสผ่านไม่ถูกต้องกรุณาลองใหม่')
        }
    }
    render(){
        const  {isLogin} = this.state
        if (!isLogin){
        return <LoginForm
            onLogin={this.onLogin}
        />
        }else{
            return <AdminPanel/>
        }
    }
}


export default AdminPage