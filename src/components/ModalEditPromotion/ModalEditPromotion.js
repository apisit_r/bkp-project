import React from 'react'
import {isEmpty}  from 'lodash'
import {Modal,Button,Container,Grid,Input, Label, TextArea} from 'semantic-ui-react'
const {Row,Column} = Grid
const AWS = require('aws-sdk');
const bluebird = require('bluebird');
const uuidv1 = require('uuid/v1');

const ENV = {
    S3_BUCKET:'bkp-picture',
    AWS_ACCESS_KEY_ID:'AKIATOAVWHVHQCAHPI2E',
    AWS_SECRET_ACCESS_KEY:'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',
    PREFIX_URL:'https://bkp-picture.s3-ap-southeast-1.amazonaws.com/'
  }
  AWS.config.update({
    accessKeyId:ENV.AWS_ACCESS_KEY_ID,
    secretAccessKey: ENV.AWS_SECRET_ACCESS_KEY,
    region: 'us-east-2',
  });
  
  // configure AWS to work with promises
  AWS.config.setPromisesDependency(bluebird);
  let reader 
class ModalEditPromotion extends React.Component{
    state={
        isCreate:true,
        date_from : '',
        date_to : '',
        full_desc : '',
        img : '',
        name : '',
        ref : '',
        short_desc : '',
        updateBy:'',
        updateTime:'',
    }
    componentDidMount(){
      const {promotionData} = this.props
      if(!isEmpty(promotionData)){
        this.setState({...promotionData,
            isCreate:false,
        })
      }
    }

    uploadFile = (buffer, name, type) => {
        const params = {
          ACL: 'public-read',
          Body: buffer,
          Bucket: ENV.S3_BUCKET,
          Key: `${name}.${type}`,
        };
        const s3 = new AWS.S3();
        const s3UploadPromise = new Promise(function(resolve, reject) {
            s3.upload(params, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
                });
        })
        const url = s3UploadPromise.then((result)=>{
            const path = `${ENV.PREFIX_URL}${name}.${type}`
            this.setState({img:path})
        })
      };
      handleEvent = (event)=>{
        const buffer =  reader.result
        const name = uuidv1()
        this.uploadFile(buffer,name, 'png')
      }
      handleUpload = (e) => {
        var file = e.target.files[0];
        reader = new FileReader();
        reader.onload = this.handleEvent
        reader.readAsArrayBuffer(file);
      }
    onSubmit(){
        const date = new Date()
        const {
            isCreate,
            date_from, 
            date_to,
            full_desc ,
            img ,
            name ,
            ref ,
            short_desc ,
            updateBy
        } = this.state;
        let submitData = {
            updateBy,
            updateTime:date,
            date_from, 
            date_to,
            full_desc ,
            img ,
            name,
            ref ,
            short_desc 
        }
        // console.log(date);
        if (isCreate){
          this.props.onSubmitNew(submitData)
        }else{
          this.props.onSubmit(submitData);
        }
        
    }
    onChange(value,field){
        this.setState({[field]:value})
    }
    render(){
        const {
            date_from, 
            date_to,
            full_desc ,
            img ,
            name ,
            ref ,
            short_desc ,
            updateBy,
            updateTime,
            isCreate
        } = this.state;
        const {isOpen,onClose,onDelete} = this.props
        return <Modal
            open={isOpen}
        >
        <Modal.Header>สร้าง / แก้ไข โปรโมชั่น</Modal.Header>
        <Modal.Content>
            <Container>
                <Grid>
                    <Row>
                        <Column fluid>
                            <Label>ชื่อโปรโมชั่น :</Label>
                            <Input
                                fluid
                                placeholder='Promotion Name' 
                                value={name}
                                onChange={(e)=>{this.onChange(e.target.value,'name')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                      <Column fluid>
                            <Label>รูปโปรโมชั่น</Label>
                            <Input
                                fluid
                                onChange={this.handleUpload}
                                type="file"/>
                            <img width={100} height={100} src={this.state.img}></img>
                        </Column>
                    </Row>
                    <Row>
                        <Column width={8}>
                                <Label>ระยะเวลาจาก :</Label>
                                <Input
                                    value={date_from}
                                    onChange={(e)=>{this.onChange(e.target.value,'date_from')}}
                                    placeholder='ระยะเวลาเริ่มต้น' 
                                 />
                        </Column>
                        <Column width={8}>
                        <Label>ระยะเวลาถึง :</Label>
                                <Input
                                    value={date_to}
                                    onChange={(e)=>{this.onChange(e.target.value,'date_to')}}
                                    placeholder='ระยะเวลาจบ' 
                                 />
                        </Column>
                    </Row>
                    <Row>
                      <Column fluid>
                            <Label>รายละเอียด</Label>
                            <TextArea
                                style={{ minHeight: 100 ,width:"100%"}}
                                value={full_desc}
                                placeholder='Full Description' 
                                onChange={(e)=>{this.onChange(e.target.value,'full_desc')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                      <Column fluid>
                            <Label>รายละเอียดแบบย่อ</Label>
                            <Input
                                fluid
                                value={short_desc}
                                placeholder='Short Description' 
                                onChange={(e)=>{this.onChange(e.target.value,'short_desc')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                      <Column fluid>
                            <Label>อ้างอิง</Label>
                            <Input
                                fluid
                                value={ref}
                                placeholder='Reference' 
                                onChange={(e)=>{this.onChange(e.target.value,'ref')}}
                            />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={6}>
                                <Label>แก้ไขโดย :</Label>
                                <Input
                                    onChange={(e)=>{this.onChange(e.target.value,'updateBy')}}
                                    placeholder='แก้ไขโดย' 
                                 />
                        </Column>
                        <Column width={10}>
                          <p>แก้ไขล่าสุด โดย {updateBy} เมื่อ {updateTime} (TIMEZONE + 7)</p>
                        </Column>
                    </Row>
                </Grid>
            </Container>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={()=>onClose()}  negative>
            ยกเลิก
          </Button>
          <Button
            positive
            labelPosition='right'
            icon='checkmark'
            onClick={()=>this.onSubmit()}
            content='บันทึก'
          />
        </Modal.Actions>
      </Modal>
    }
}

export default ModalEditPromotion