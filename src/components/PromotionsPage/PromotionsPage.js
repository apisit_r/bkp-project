import React from 'react'
import {
    Menu,
    Container,
    Grid,
    Tab,
    GridColumn,
    GridRow,
    Segment,
    Button,
    Icon,
    Image,
    Row,
  } from 'semantic-ui-react';
import _ from 'lodash';
import PromotionCard from "../PromotionCard";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import { getPromotions } from './PromotionsActions';
import { selectPromotions } from './PromotionsReducer';
const styled = require('styled-components').default;


const PromotionsPageStyle = styled.div `

.no-padding-top{
    padding-top:0 !important;
}
.marginBottom10{
    margin-bottom:10px;
}
.text-header {
    font-family: 'Kanit', sans-serif; 
    font-size: 18px
    font-weight: bold;
    border-bottom:3px solid #E94200;
    margin-bottom:5px;
}
.card{
    padding-bottom:50px;
}
`


class PromotionsPage extends React.Component {
    constructor(props){
        super(props)
    }

    componentWillMount(){
        this.props.getPromotions();
    }

    renderPromotionsCard = (promotions)=>{
        return promotions.reverse().map((promotion)=>(
            <Link href={`/promotion/${promotion.key}`}>
                <GridRow>
                    <div className="card">
                        <PromotionCard
                        id={promotion.id}
                        dateFrom={promotion.date_from}
                        dateTo={promotion.date_to}
                        img={promotion.img}
                        shortDesc={promotion.short_desc}
                        />
                    </div>
                </GridRow>
            </Link>
        ))
    }
    render(){
        const {promotions} = this.props
        return (
            <PromotionsPageStyle>
                <Container>
                    <Grid>
                        <GridRow>
                            <p style={{fontSize:'10px'}}>หน้าแรก > โปรโมชั่น</p>
                        </GridRow>
                        <GridColumn width={16} className='no-padding-top marginBottom10'>
                            <GridRow>
                                <span className="text-header">ข่าวสารและกิจกรรม</span>
                            </GridRow>
                        </GridColumn>
                        <GridColumn width={16}>
                            {this.renderPromotionsCard(promotions)}
                        </GridColumn>
                    </Grid>
                </Container>
            </PromotionsPageStyle>
        )
    }
}

const mapStateToProps = (state) => ({
    promotions: selectPromotions(state),
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        getPromotions,
    }, dispatch)
  );

export default connect(mapStateToProps, mapDispatchToProps)(PromotionsPage);