import React from 'react';
import PropTypes from 'prop-types';
import {
    Menu,
    Container,
    Grid,
    Tab,
    GridColumn,
    GridRow,
    Segment,
    Button,
    Icon,
    Image,
    Card,
    Input,
  } from 'semantic-ui-react';
import ModalEditContent from '../ModalEditContent'
import ModalEditCooking from '../ModalEditCooking'
import ModalEditPromotion from '../ModalEditPromotion'
import firebase from "firebase";

class AdminPanel extends React.Component {
    state = {
        isOpen:false,
        sku: '',
        productData: {},
        key: '',
        cookingData:[],
        promotionData:[],
        isOpenCooking:false,
        isOpenPromotion:false
    }
    constructor(){
        super()
    }

    componentWillMount(){
        const config = {
            apiKey: "AIzaSyDu9UtZIdt0SmKZW-3gQvBtQ22Usr21EYM",
            authDomain: "bkp-project.firebaseapp.com",
            databaseURL: "https://bkp-project.firebaseio.com",
            projectId: "bkp-project",
            storageBucket: "bkp-project.appspot.com",
            messagingSenderId: "464649577867"
          };
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        
    }

    componentDidMount(){
        const cookingData = []
        const promotionData = []
        const database = firebase.database()
        database.ref('/cooking').once('value',(dataSnapshot) => {
            dataSnapshot.forEach(function(item) {
                var itemVal = item.val();
                cookingData.push({data:itemVal,key:item.key});
            });
          })
          database.ref('/promotions').once('value').then(function(dataSnapshot) {
            dataSnapshot.forEach(function(item) {
                var itemVal = item.val();
                promotionData.push({data:itemVal,key:item.key});
            });
          })
        this.setState({cookingData,promotionData})
    }

    onClose = () => {
        this.setState({isOpen:false,isOpenCooking:false,isOpenPromotion:false})
    }
    onSubmit = (key) => (data) => {
        // console.log(data)
        const database = firebase.database()
        try{
            database.ref('/product/'+key).update(data)
            alert('บันทึกสำเร็จ')
            this.onClose()
        }
        catch(e){
            alert('โปรดใส่ ชื่อผู้แก้ไข')
        }

    }
    onDelete = (key) => () => {
        const isConfirm = window.confirm("ต้องการลบสินค้าชิ้นนี้")
        if(isConfirm){
            const database = firebase.database()
            database.ref('/product/'+key).remove()
            alert('ลบสินค้าเรียบร้อย')
            this.onClose();
        }
    }
    onSubmitNew = (data) =>{
        // console.log(data)
        const database = firebase.database()
        try{
            database.ref('/product').push().set(data)
            alert('บันทึกสำเร็จ')
            this.onClose()
        }
        catch(e){
            alert('โปรดใส่ ชื่อผู้แก้ไข')
        }
    }
    createCooking = data =>{
        const database = firebase.database()
        try{
            database.ref('/cooking').push().set(data)
            alert('บันทึกสำเร็จ')
            this.onClose()
        }
        catch(e){
            alert('โปรดใส่ ชื่อผู้แก้ไข')
        }
    }
    updateCooking = (key)=> data =>{
        const database = firebase.database()
        try{
            database.ref('/cooking/'+key).update(data)
            alert('บันทึกสำเร็จ')
            this.onClose()
        }
        catch(e){
            alert('โปรดใส่ ชื่อผู้แก้ไข')
        }
    }
    deleteCooking = (key) =>{
        const isConfirm = window.confirm("ต้องการลบข้อมูลหรือไม่")
        if(isConfirm){
            const database = firebase.database()
            database.ref('/cooking/'+key).remove()
            alert('ลบสินค้าเรียบร้อย')
            this.onClose();
        }
    }
    createPromotion = data =>{
        const database = firebase.database()
        try{
            database.ref('/promotions').push().set(data)
            alert('บันทึกสำเร็จ')
            this.onClose()
        }
        catch(e){
            alert('โปรดใส่ ชื่อผู้แก้ไข')
        }
    }
    updatePromotiuon = key => data =>{
        const database = firebase.database()
        try{
            database.ref('/promotions/'+key).update(data)
            alert('บันทึกสำเร็จ')
            this.onClose()
        }
        catch(e){
            alert('โปรดใส่ ชื่อผู้แก้ไข')
        }
    }
    deletePromotion = key =>{
        console.log('key',key)
        const isConfirm = window.confirm("ต้องการลบข้อมูลหรือไม่")
        if(isConfirm){
            const database = firebase.database()
            database.ref('/promotions/'+key).remove()
            alert('ลบโปรโมชั่น')
            this.onClose();
        }
    }
    onClickSearch = () =>{
        this.setState({productData: {}})
        const productId = this.state.sku
        const database = firebase.database()
        database.ref('/product').orderByChild("productId").equalTo(parseInt(productId)).once('value')
        .then((dataSnapshot) => {
            const data = dataSnapshot.val()
            if(data){
                for (let [key, value] of Object.entries(data)) {
                    this.setState({productData: value, isOpen: true,key:key})
                }
            } else {
                alert('Can\'t find this product')
                this.setState({sku:''})
            }
        })
    }
    handleChange = (e, { name, value }) => {
        this.setState({ [name]: value })
    }
    renderProductPanel = () =>
    {
        const {isOpen,sku,productData,key} =this.state
        return (
            <Container>
                { isOpen && <ModalEditContent 
                isOpen={isOpen}
                onClose={this.onClose}
                onSubmit={this.onSubmit(key)}
                onDelete={this.onDelete(key)}
                onSubmitNew={this.onSubmitNew}
                productData={productData}
                />}
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <h1>Product Management</h1>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width='10'>
                            <Input 
                            fluid 
                            placeholder='Search SAP Code...' 
                            name='sku'
                            value={sku}
                            onChange={this.handleChange}
                            />
                        </Grid.Column>
                        <Grid.Column width='3'>
                            <Button
                            content='Search'
                            fluid
                            onClick={this.onClickSearch}
                            />
                        </Grid.Column>
                        <Grid.Column width='3'>
                            <Button
                            content='Add'
                            fluid
                            onClick={()=>{this.setState({productData:{},isOpen:true})}}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
    )}
    renderCookingPagePanel = () => {
        const {cookingData,cookingEdit={},isOpenCooking} =this.state
        return (<Container>
               { isOpenCooking && <ModalEditCooking 
                isOpen={isOpenCooking}
                onClose={this.onClose}
                onSubmitNew={this.createCooking}
                onSubmit={this.updateCooking(cookingEdit.key)}
                cookingData={cookingEdit.data}
                />
            }
        <Grid>
            <Grid.Row>
                <Grid.Column width='9'>
                    <h1>Cooking Management</h1>
                </Grid.Column>
                <Grid.Column width='3'>
                            <Button
                            content='Add new Cooking'
                            fluid
                            onClick={()=>{this.setState({isOpenCooking:true,cookingEdit:{}})}}
                            />
                 </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                {cookingData.map((cookingItem)=>(
                    <Card fluid>
                        <Card.Content>
                            <Card.Header>{cookingItem.data.name}</Card.Header>
                        </Card.Content>
                        <Card.Content extra>
                            <div className='ui two buttons'>
                            <Button basic color='green'
                            onClick = {()=>{this.setState({isOpenCooking:true,cookingEdit:cookingItem})}}>
                                EDIT
                            </Button>
                            <Button basic color='red'
                            onClick={()=>{this.deleteCooking(cookingItem.key)}}>
                                DELETE
                            </Button>
                            </div>
                        </Card.Content>
                    </Card>
                ))}
            </Grid.Row>
        </Grid>
    </Container>)
    }
    renderPromotionPagePanel = () => {
        const {promotionData,promotionEdit={},isOpenPromotion} =this.state
        return (<Container>
               { isOpenPromotion && <ModalEditPromotion 
                isOpen={isOpenPromotion}
                onClose={this.onClose}
                onSubmitNew={this.createPromotion}
                onSubmit={this.updatePromotiuon(promotionEdit.key)}
                promotionData={promotionEdit.data}
                />
            }
        <Grid>
            <Grid.Row>
                <Grid.Column width='9'>
                    <h1>Promotions Management</h1>
                </Grid.Column>
                <Grid.Column width='3'>
                            <Button
                            content='Add new Prmotions'
                            fluid
                            onClick={()=>{this.setState({isOpenPromotion:true,promotionEdit:{}})}}
                            />
                 </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                {promotionData.map((promotion)=>(
                    <Card fluid>
                        <Card.Content>
                            <Card.Header>{promotion.data.name}</Card.Header>
                        </Card.Content>
                        <Card.Content extra>
                            <div className='ui two buttons'>
                            <Button basic color='green'
                            onClick = {()=>{this.setState({isOpenPromotion:true,promotionEdit:promotion})}}>
                                EDIT
                            </Button>
                            <Button basic color='red'
                            onClick={()=>{this.deletePromotion(promotion.key)}}>
                                DELETE
                            </Button>
                            </div>
                        </Card.Content>
                    </Card>
                ))}
            </Grid.Row>
        </Grid>
    </Container>)
    }
    render() {
        
        const panes = [
            { menuItem: 'Product', render: () => <Tab.Pane>{this.renderProductPanel()}</Tab.Pane> },
            { menuItem: 'Cooking Content', render: () => <Tab.Pane>{this.renderCookingPagePanel()}</Tab.Pane> },
            { menuItem: 'Promotion', render: () => <Tab.Pane>{this.renderPromotionPagePanel()}</Tab.Pane> },
          ]
        return (
            <Container style={{paddingTop:'50px'}}><Tab panes={panes} /></Container>
        )
    }
}

export default (AdminPanel);