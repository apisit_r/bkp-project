import React from 'react'
import {
    Menu,
    Container,
    Grid,
    Tab,
    GridColumn,
    GridRow,
    Segment,
    Button,
    Icon,
    Image,
    Row,
  } from 'semantic-ui-react';
import _ from 'lodash';
import Slider from "react-slick";
import PromotionCard from "../PromotionCard";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link,Router } from 'react-router';
import { getPromotions } from '../PromotionsPage/PromotionsActions';
import { selectPromotions } from '../PromotionsPage/PromotionsReducer';
const styled = require('styled-components').default;


const PromotionsPageStyle = styled.div `
.text-bold{
    font-weight: bold;
}
.text-header{
    font-family: 'Kanit', sans-serif; 
    font-size: 18px
    font-weight: bold;
    border-bottom:3px solid #E94200;    
}
.text-header-bottom{
    font-family: 'Kanit', sans-serif; 
    font-size: 12px
    font-weight: bold;
    color: #EA4200;
}
.icon-button{
    width: 10%;
    margin: auto 10px;
    top: -1px;
    display: inline-block !important;
}
.text-sub-header-bottom{
    font-family: 'Kanit', sans-serif; 
    font-size: 21px;
}
.no-padding-top-bottom{
    padding-top:0 !important;
    padding-bottom:0 !important;
}
.promo-detail{
    margin: 0rem 1rem 1rem 1rem;
    border-radius:0px 0px 25px 25px;
    box-shadow: 0px 5px 5px 4px #D1D1D1;
    min-height:250px;
    background:white;
}
.promo-detail-img{
    height: 345px;
    margin: auto;
}
div.wrapper{
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    width: 600px;
    height: 200px;
}
.promo-ref{
    margin-top
    position: absolute;
    bottom: 15px;
    right: 20px;
    max-width: 65%;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

.slick-prev{
    z-index:10;
}
.slick-dots li{
    margin:0px;
}
.slick-next{
    right:20px!important;
}
.news-promo{
    padding-top: 30px;
    text-align:center;
    b{
        color:#EA4200;
        font-size:24px;
        line-height:1.5;
    }
    p{
        font-size:21px;
    }
}
.sub-header {
    font-family: 'Kanit', sans-serif; 
    font-size: 15px;
    line-height:1.5;
}
.orange-text{
    color: #EA4200;
}
.slick-next::before{
    display:none;
}
.slick-prev::before{
    display:none;
}
.icon-promo{
    width:60px;
}
`

class RightNavButton extends React.Component {
    render() {
      return <button {...this.props}><img
                className='icon-promo'
                src='https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/promotion_logo/right.png'
                />
            </button>  
    }
  }
  class LeftNavButton extends React.Component {
    render() {
      return <button {...this.props}><img
                className='icon-promo'
                src='https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/promotion_logo/left.png'
                />
            </button>  
    }
  }
class PromotionsDetail extends React.Component {
    constructor(props){
        super(props)
    }

    componentWillMount(){
        const { promotions } = this.props
        if(promotions.length <= 0){
            this.props.getPromotions();
        }
    }

    renderRelatePromotions = (promotions) => {
        const settings = {
            infinite: true,
            slidesToShow: 1,
            dots:true,
            speed:2000,
            duration:0,
          };
        return (
            <Slider className="slider-relate-promo"
            {...settings}>
                {promotions.reverse().map((promotion)=>(
                    <Link href={`/promotion/${promotion.key}`}>
                        <div style={{padding:'20px 5px 25px 5px'}}>
                                <PromotionCard
                                id={promotion.key}
                                dateFrom={promotion.date_from}
                                dateTo={promotion.date_to}
                                img={promotion.img}
                                shortDesc={promotion.short_desc}
                                />
                            </div>
                    </Link>
                ))}
            </Slider>
        )}
    render(){
        const key = this.props.params.id;
        const {promotions} = this.props
        const currentPromo = promotions.find((promo)=>(promo.key===key));
        if(promotions.length===0){
            return null;
        }
        const relatePromo = promotions.filter((promo)=>(promo.key!==key));
        const {
            name,
            img,
            date_to,
            date_from,
            full_desc,
            ref
        } = currentPromo
        return (
            <PromotionsPageStyle>
                <Container>
                    <Grid>
                        <GridRow>
                            <p style={{fontSize:'12px'}} >  หน้าแรก > ข่าวสารและกิจกรรม > {name}</p>
                        </GridRow>
                        <GridColumn width={16} >
                            <GridRow>
                                <span className="text-header">โปรโมชั่นพิเศษจากเรา</span>
                            </GridRow>
                        </GridColumn>
                    </Grid>
                    <Grid>
                        <GridRow  style={{"backgroundColor":"#E94200","padding":'0px',"zIndex":'10000'}}>
                                <img className="promo-detail-img" src={img}/>
                        </GridRow>
                        <GridColumn className="promo-detail" width={16}>
                            <GridRow style={{marginBottom:'10px'}}>
                                <Image
                                    src='https://s3-ap-southeast-1.amazonaws.com/www.bkpthailand.com/promotion_logo/calendar.jpg'
                                    className='icon-button'
                                />
                                <span className="sub-header text-bold" style={{color:'red'}}>|</span> 
                                <span  className="sub-header text-bold">  {date_from} - {date_to}</span>
                            </GridRow>
                            <GridRow style={{paddingBottom: "35px"}}>
                                <b className="sub-header" style={{"fontSize": "17px"}}>{name}</b>
                                <p className="sub-header">{full_desc}</p>
                            </GridRow>
                            <GridRow className="promo-ref">
                               <span className="sub-header"> <span className='orange-text'>ที่มา: </span> <a href={ref}>{ref}</a> </span> 
                            </GridRow>
                        </GridColumn>
                    </Grid>
                    <Grid>
                        <GridColumn className="news-promo" width={16}>
                            <GridRow>
                                <b className='text-header-bottom'>ข่าวสารและกิจกรรมเพิ่มเติม</b>
                            </GridRow>
                            <GridRow className='no-padding-top-bottom'>
                                <p className='text-sub-header-bottom'>ไม่พลาดทุกการอัพเดทได้ที่นี้</p>
                            </GridRow>
                        </GridColumn>
                    </Grid>
                    <div className="relate-promo">
                        {this.renderRelatePromotions(relatePromo)}
                    </div>
                </Container>
            </PromotionsPageStyle>
        )
    }
}

const mapStateToProps = (state) => ({
    promotions: selectPromotions(state),
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        getPromotions,
    }, dispatch)
  );

export default connect(mapStateToProps, mapDispatchToProps)(PromotionsDetail);
// export default PromotionsDetail;